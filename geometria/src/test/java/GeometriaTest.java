package geometria;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import static org.junit.jupiter.api.Assertions.*;

public class GeometriaTest {

	private static WebDriver driver;

	/*
	 * ENTRADA ---> CATETO1 = 3 | CATETO2 = 4
	 * SA�DA ---> HIPOTENUSA = 5
	 */
	@Test
	public void realizarCalculoHipotenusaTest() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		driver = new ChromeDriver();

		driver.get("file:///C:/Users/w10-b/Downloads/triangulo.html");

		new Select(driver.findElement(By.id("tipoCalculoSelect"))).selectByVisibleText("Hipotenusa");

		WebElement inputCateto1 = driver.findElement(By.id("cateto1"));
		WebElement inputCateto2 = driver.findElement(By.id("cateto2"));
		WebElement botaoCalcular = driver.findElement(By.id("calcularBtn"));

		inputCateto1.sendKeys("3" + Keys.ENTER);
		inputCateto2.sendKeys("4" + Keys.ENTER);

		botaoCalcular.click();

		String valorAtual = driver.findElement(By.id("hipotenusa")).getAttribute("value");
		String valorEsperado = "5";

		assertEquals(valorEsperado, valorAtual);
	}

	/*
	 * ENTRADA ---> CATETO1 = 6 | HIPOTENUSA = 10
	 * SA�DA ---> CATETO2 = 8
	 */
	@Test
	public void realizarCalculoCatetoTest() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");

		driver = new FirefoxDriver();

		driver.get("file:///C:/Users/w10-b/Downloads/triangulo.html");

		new Select(driver.findElement(By.id("tipoCalculoSelect"))).selectByVisibleText("Cateto");

		WebElement inputCateto1 = driver.findElement(By.id("cateto1"));
		WebElement inputHipotenusa = driver.findElement(By.id("hipotenusa"));
		WebElement botaoCalcular = driver.findElement(By.id("calcularBtn"));

		inputCateto1.sendKeys("6" + Keys.ENTER);
		inputHipotenusa.sendKeys("10" + Keys.ENTER);

		botaoCalcular.click();

		String valorAtual = driver.findElement(By.id("cateto2")).getAttribute("value");
		String valorEsperado = "8";

		assertEquals(valorEsperado, valorAtual);
	}

	@AfterAll
	public static void fecharDriver() {
		driver.quit();
	}

}